# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="git://anongit.kde.org/oxygen-gtk"
    require scm-git
else
    DOWNLOADS="mirror://kde/stable/${PN}/${PV}/src/${PNV}.tar.bz2"
fi

require cmake

SUMMARY="GTK+2 port of KDE's Oxygen theme"
DESCRIPTION="
Oxygen-gtk is a port of the default KDE widget theme Oxygen to GTK+. Its
primary goal is to ensure visual consistency between GTK+ and Qt-based
applications running under KDE. A secondary objective is to also have a
standalone nice looking GTK+ theme that would behave well under other desktop
environments.

Unlike other attempts made to port the KDE Oxygen theme to GTK+ this does not
depend on Qt (via some Qt to GTK+ conversion engine), nor does it render widget
appearance via hard-coded pixmaps (which breaks every time some setting is
changed in KDE).
"
HOMEPAGE="https://projects.kde.org/projects/playground/artwork/oxygen-gtk"

LICENCES="LGPL-2.1"
SLOT="0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/atk
        dev-libs/dbus-glib:1 [[ note = [ Dependency is automagic ] ]]
        dev-libs/glib:2
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2[>=2.24.2]
        x11-libs/libX11
        x11-libs/pango
    suggestion:
        kde/kde-gtk-config [[ description = [
            KDE configuration module to configure appearance of GTK applications
        ] ]]
        lxde-desktop/lxappearance [[ description = [
            Simple GUI to modify your ~/.gtkrc if you don't have GNOME installed
        ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DINSTALL_PATH_GTK_THEMES=/usr/share/themes
)

