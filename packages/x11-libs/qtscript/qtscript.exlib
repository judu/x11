# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtScript"
DESCRIPTION="Classes for making Qt applications scriptable with ECMAScript. Provided for Qt 4.x
compatibility. For new code the QJS* classes in the QtQml module should be used."

LICENCES+="
    GPL-2
    LGPL-2 [[ note = [ JavaScriptCore ] ]]
"
MYOPTIONS="
    examples
    tools    [[ description = [ Build the Qt Script Tools embeddable debugger ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][?gui(+)]
        tools? ( x11-libs/qtbase:${SLOT}[gui(+)] )
"

qtscript_src_configure()  {
    # TODO: figure out a way to disable these things sanely
    option tools || edo sed -e "/scripttools/d" -i src/src.pro

    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

