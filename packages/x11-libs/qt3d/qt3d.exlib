# Copytight 2024 Ole Salscheider <ole@salscheider.org>
# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: API for 2D and 3D rendering"
DESCRIPTION="
Qt 3D provides functionality for near-realtime simulation systems with support
for 2D and 3D rendering in both Qt C++ and Qt Quick applications."

DEPENDENCIES="
    build+run:
        media-libs/assimp[>=5.0.0]
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtmultimedia:${SLOT}[>=${PV}]
        x11-libs/qtshadertools:${SLOT}[>=${PV}]
        sys-libs/zlib
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DQT_FEATURE_qt3d_system_assimp=ON
)

qt3d_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qt3d_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

