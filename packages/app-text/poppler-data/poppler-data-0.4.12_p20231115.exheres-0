# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

ADOBE_CMAP_VERSION=${PV##*_p}
MY_PV=${PV%_p*}
MY_PNV=${PN}-${MY_PV}

SUMMARY="Encoding data for poppler"
HOMEPAGE="https://poppler.freedesktop.org"
DOWNLOADS="
    ${HOMEPAGE}/${MY_PNV}.tar.gz
    https://github.com/adobe-type-tools/cmap-resources/archive/refs/tags/${ADOBE_CMAP_VERSION}.tar.gz -> adobe-cmap-${ADOBE_CMAP_VERSION}.tar.gz
"

LICENCES="adobe-ps GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_INSTALL_PARAMS=( prefix="/usr" )

src_install() {
    default

    # Install extra files from Adobe that poppler-data didn’t
    # include yet or won’t ever include
    # Fixes Ghostscript for some files, see https://bugs.gentoo.org/844115
    edo cd "${WORKBASE}"/cmap-resources-${ADOBE_CMAP_VERSION}

    # These won’t ever be included (according to Debian, see Gentoo bug report)
    insinto /usr/share/poppler/cMap
    doins Adobe-Identity-0/CMap/*

    # These are new files not in a poppler-data release yet
    insinto /usr/share/poppler/cMap/Adobe-GB1
    doins Adobe-GB1-6/CMap/Adobe-GB1-6
    insinto /usr/share/poppler/cMap/Adobe-Manga1-0
    doins Adobe-Manga1-0/CMap/*
}
