# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ subdir=${PNV} suffix=tar.gz ] xfont

SUMMARY="The GNU Unifont and a collection of utilities from unifoundry.com"
HOMEPAGE="https://unifoundry.com"
DOWNLOADS="${HOMEPAGE}/pub/${PN}/${PNV}/${PNV}.tar.gz"

LICENCES="|| ( freedist GPL-2-font-exception GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    utils [[ description = [ Include additional font utilities ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        utils? (
            dev-lang/perl:*
            dev-perl/GD
        )
        !font/unifont-ttf [[ description = [ Installs the same ttf font ]
                             resolution = manual ]]
"

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX="${IMAGE}"/usr/$(exhost --target)
    PKGDEST="${IMAGE}"/usr/share/${PN}
    PCFDEST="${IMAGE}"/usr/share/fonts/X11/${PN}
    TTFDEST="${IMAGE}"/usr/share/fonts/X11/${PN}
)
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/unifont.{info,txt} )

src_prepare() {
    default

    # Respect CC, do not strip on install and respect CFLAGS
    edo sed \
        -e "s:gcc:${CC}:" \
        -e "s/ -s//g" \
        -e "s/^CFLAGS =.*/CFLAGS += -Wall/" \
        -i src/Makefile

    # we install man pages and fonts seperately in src_install
    edo sed \
        -e '/man install/d' \
        -e '/font install/d' \
        -i  Makefile
}

src_install() {
    # install font
    emake DESTDIR="${IMAGE}" "${DEFAULT_SRC_INSTALL_PARAMS[@]}" PREFIX="${IMAGE}"/usr -C font install

    if option utils ; then
        default

        # install man pages
        emake DESTDIR="${IMAGE}" "${DEFAULT_SRC_INSTALL_PARAMS[@]}" PREFIX="${IMAGE}"/usr -C man install
        edo rm "${IMAGE}"/usr/share/${PN}/*.gz
    fi

    emagicdocs
}

